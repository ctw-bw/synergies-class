#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <locale.h>
#include <map>
#include "MEP_CEINMS.hpp"



MEP_class::MEP_class()
{
    setlocale(LC_NUMERIC, "C");
    MEP_class::importRegression_coef();
    MEP_class::import_gaussXP();
    MEP_class::importBaseline();
}


void MEP_class::import_gaussXP()
{

    try
    {
        std::ifstream data("gaussXP.txt");
        //data.exceptions(ifstream::failbit | ifstream::badbit);
        //int i = 0;
        //int j = 0;
        std::string line;
        std::string cell;
        double cell_double = 0;

       // cout << "importing XP" << endl;
        while (getline(data, line))
        {

            std::stringstream lineStream(line);
            std::string cell;
            std::vector<double> myVec;
            while (getline(lineStream, cell, ','))
            {

                cell_double = atof(cell.c_str());
                myVec.push_back(cell_double);
                //j++;
                //cout << cell.c_str() << "--" << cell_double << endl;
            }
            gaussianCoef.push_back(myVec);
            myVec.erase(myVec.begin(), myVec.end());
            // i++;
            //j = 0;
        }
        data.close();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << e.what() << std::endl;
    }
}

void MEP_class::importRegression_coef()
{
    try
    {
        std::ifstream data("regression.txt");
        //data.exceptions(ifstream::failbit | ifstream::badbit);
        //int i = 0;
        //int j = 0;
        std::string line;
        std::string cell;
        double cell_double = 0;

       // cout << "importing regression " << endl;
        while (getline(data, line))
        {
            std::stringstream lineStream(line);
            std::string cell;
            std::vector<double> myVec;
            while (getline(lineStream, cell, ','))
            {
                cell_double = atof(cell.c_str());
                myVec.push_back(cell_double);
                //j++;
                // cout << "reg entered" << endl;
            }
            regression_coef.push_back(myVec);
            myVec.erase(myVec.begin(), myVec.end());
            //i++;
            //j = 0;
        }
        data.close();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << e.what() << std::endl;
    }
}

void MEP_class::importBaseline()
{
    try
    {
        std::ifstream data("weightings.txt");
        //data.exceptions(ifstream::failbit | ifstream::badbit);
        int i = 0;
        int j = 0;
        std::string line;
        std::string cell;
        double cell_double = 0;

       // cout << "importing baseline" << endl;
        while (getline(data, line))
        {
            std::stringstream lineStream(line);
            std::string cell;
            std::vector<double> myVec;

            while (getline(lineStream, cell, ','))
            {
                cell_double = atof(cell.c_str());
                myVec.push_back(cell_double);
                //j++;
                //cout << j << " - " << i << endl;
            }
            weightings_baseline.push_back(myVec);
            myVec.erase(myVec.begin(), myVec.end());
            //i++;
            //j = 0;
        }
        data.close();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << e.what() << std::endl;
    }
}


//function to calculate the continuous gaussians
void MEP_class::calcGaussians()
{

    //I need to have a continuous function for each component
    double size = gaussianCoef[0].size();

    double percentage = 100 / size; //all the components have fixed amount of points

    std::vector<double> myVec;

    for (int i = 1; i <= gaussianCoef[0].size(); i++)

        myVec.push_back(i*percentage);
    int size2 = gaussianCoef.size();

    for (int i = 0; i < gaussianCoef.size(); i++)
    {

        Curve<CurveMode::Offline> mySpline(myVec, gaussianCoef[i]);
    
        gaussXP.push_back(mySpline);
    }

    
}

void MEP_class::calcWeightings(int elevation, int speed)
{
    double elevation_temp = 0.0;
    double speed_temp = 0.0;
    double correction = 0.0;
    double constant = 0.0;
    std::vector<double> myVec;

    for (int comp = 1; comp <= 4; comp++)
    {
        for (int musc = 1; musc <= regression_coef.size(); musc++)
        {
            /* get the correction value at baseline: elevation =0, speed = 3*/
            //cout << "-------------------" << musc << comp << endl;
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 1)];
            elevation_temp = constant * (0.0 * 0.0);
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 2)];
            elevation_temp += (constant)*0.0;
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 3)];
            elevation_temp += constant;

            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 1)];
            speed_temp = constant * (3.0 * 3.0);
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 2)];
            speed_temp += (constant)*3.0;
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 3)];
            speed_temp += constant;

            correction = elevation_temp + speed_temp;

            /* get the weighting value */
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 1)];
            elevation_temp = constant * (elevation * elevation);
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 2)];
            elevation_temp += (constant)*elevation;
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("elevation", comp, 3)];
            elevation_temp += constant;

            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 1)];
            speed_temp = constant * (speed * speed);
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 2)];
            speed_temp += (constant)*speed;
            constant = MEP_class::regression_coef[musc - 1][MEP_class::w_pointer("speed", comp, 3)];
            speed_temp += constant;

            myVec.push_back(MEP_class::weightings_baseline[musc - 1][comp - 1] + elevation_temp + speed_temp - correction);

            /* Deals with negative values */
            if (myVec[musc - 1] < 0)
                myVec[musc - 1] = 0;

            //cout << " speed: " << speed_temp << " elevation: " << elevation_temp << endl;
            //cout << "baseline " << MEP_class::weightings_baseline[comp-1][musc-1] << endl;
            //std::cout << "weighting: " << MEP_class::weightings[comp-1][musc-1]<< std::endl;

            //cin >> key;
        }
        weightings.push_back(myVec);
        myVec.erase(myVec.begin(), myVec.end());
    }
}

int MEP_class::w_pointer(std::string cond, int comp, int val)
{
    if (cond == "elevation")
    {
        return (comp - 1) * 3 + val - 1;
    }

    if (cond == "speed")
    {
        return (comp - 1) * 3 + 12 + val - 1;
    }

    return -1;
}

void MEP_class::calcMEP(int elevation, int speed, std::vector<double>& MEP, double time)
{
    MEP_class::calcWeightings(elevation, speed);

    //secondary when I will implement the xml file //std::map<std::string, double> mapMuscle;//in order to have the same order as the muscle model I first need to map the data from what I read

    double excitation = 0;

    for (int musc = 0; musc < regression_coef.size(); musc++)
    {
        for (int comp = 0; comp < 4; comp++)
        {
            double value = MEP_class::gaussXP[comp].getValue(time);
            excitation += MEP_class::gaussXP[comp].getValue(time) * MEP_class::weightings[comp][musc];
        }
        MEP.push_back(excitation);
        excitation = 0;
    }

    // Sort the excitation data to have the same order as the muscles from the model
    // for (vector<string>::const_iterator it1 = muscleNames_.begin(); it1 != muscleNames_.end(); it1++)
    // {
    //     if (mapMuscle.find(*it1) != mapMuscle.end())
    //     {
    //         MEP.push_back(mapMuscle.at(*it1));
    //         //std::cout << *it1 << ": " << mapMuscle.at ( *it1 ) << std::endl;
    //     }
    //     else
    //         MEP.push_back(0);
    // }
 }

 int indexMuscle(std::string muscle)
 {
     if (muscle == "TA")
         return 1;
     else
         return 0;
}

// int MEP_class::getMuscle_Index(std::string muscle)
// {
//     if (muscle == "TA")
//     {
//         return 0;
//     }
//     else if (muscle == "Sol")
//     {
//         return 1;
//     }
//     else if (muscle == "Per")
//     {
//         return 2;
//     }
//     else if (muscle == "VastLat")
//     {
//         return 3;
//     }
//     else if (muscle == "VastMed")
//     {
//         return 4;
//     }
//     else if (muscle == "RFem")
//     {
//         return 5;
//     }
//     else if (muscle == "Sar")
//     {
//         return 6;
//     }
//     else if (muscle == "Add")
//     {
//         return 7;
//     }
//     else if (muscle == "GlutMed")
//     {
//         return 8;
//     }
//     else if (muscle == "TFL")
//     {
//         return 9;
//     }
//     else if (muscle == "GastLat")
//     {
//         return 10;
//     }
//     else if (muscle == "GastMed")
//     {
//         return 11;
//     }
//     else if (muscle == "BFem")
//     {
//         return 12;
//     }
//     else if (muscle == "Semi")
//     {
//         return 13;
//     }
//     else if (muscle == "GlutMax")
//     {
//         return 14;
//     }
// }

// void getMEP_all(int gaitCycle, double *data)
// {
//     //memcpy(data, MEP_class::mep[gaitCycle][1], 15);
// }

// void MEP_class::saveXP()
// {
//     ostringstream sstream;
//     string saveText = "";

//     cout << "saving XP" << endl;
//     for (int i = 0; i < 4; i++)
//     {
//         for (int j = 0; j < 200; j++)
//         {
//             sstream << MEP_class::gaussXP[j][i] << ",";
//         }
//         sstream << endl;
//     }
//     saveText = sstream.str();
//     ofstream saveFile("save/XP.txt");
//     saveFile << saveText;

//     saveFile.close();
// }

// void MEP_class::saveWeightings()
// {
//     ostringstream sstream;
//     string saveText = "";

//     cout << "saving weigthings" << endl;
//     for (int i = 0; i < 4; i++)
//     {
//         for (int j = 0; j < 15; j++)
//         {
//             sstream << MEP_class::weightings[j][i] << ",";
//         }
//         sstream << endl;
//     }
//     saveText = sstream.str();
//     ofstream saveFile("save/weightings.txt");
//     saveFile << saveText;

//     saveFile.close();
// }

// void MEP_class::saveMEP()
// {
//     ostringstream sstream;
//     string saveText = "";

//     cout << "saving MEP" << endl;
//     for (int j = 0; j < 200; j++)
//     {
//         for (int i = 0; i < 15; i++)
//         {
//             sstream << MEP_class::mep[j][i] << ",";
//         }
//         sstream << endl;
//     }
//     saveText = sstream.str();
//     ofstream saveFile("save/mep.txt");
//     saveFile << saveText;

//     saveFile.close();
// }

// void MEP_class::saveData(double data[])
// {
//     ostringstream sstream;
//     string saveText = "";

//     cout << "saving data" << endl;
//     for (int i = 0; i < 400; i++)
//     {
//         sstream << data[i] << ",";
//     }

//     sstream << endl;
//     saveText = sstream.str();
//     ofstream saveFile("save/data_out.txt");
//     saveFile << saveText;

//     saveFile.close();
// }
