//==============================================================
// Filename : MAP_model.cpp
// Authors : Federica Damonte
// Description :  Generates an estimate of Synergies and Muscle Excitation Profiles (MEP)
// Based on speed, elevation and baseline. 
// 
// There are 2 baseline modes: 'GM', which is the generic mode and no file
// should be indicated and 'SM', which is the subject-specific mode and 
// a file should be indicated in the next argument (baseline_file)
//
// The function outpus and estimate of the weights and the XP for the 
// synergies and the estimated MEPs.
//==============================================================

#include <string>
#include "MAP_model.hpp"
#include <iostream>
#include <fstream>
#include <vector>

Eigen::MatrixXd openData(std::string fileToOpen)
{

    // the inspiration for creating this function was drawn from here (I did NOT copy and paste the code)
    // https://stackoverflow.com/questions/34247057/how-to-read-csv-file-and-assign-to-eigen-matrix

    // the input is the file: "fileToOpen.csv":
    // a,b,c
    // d,e,f
    // This function converts input file data into the Eigen matrix format

    // the matrix entries are stored in this variable row-wise. For example if we have the matrix:
    // M=[a b c
    //	  d e f]
    // the entries are stored as matrixEntries=[a,b,c,d,e,f], that is the variable "matrixEntries" is a row vector
    // later on, this vector is mapped into the Eigen matrix format
    std::vector<double> matrixEntries;

    // in this object we store the data from the matrix
    std::ifstream matrixDataFile(fileToOpen);

    // this variable is used to store the row of the matrix that contains commas
    std::string matrixRowString;

    // this variable is used to store the matrix entry;
    std::string matrixEntry;

    // this variable is used to track the number of rows
    int matrixRowNumber = 0;

    while (getline(matrixDataFile, matrixRowString)) // here we read a row by row of matrixDataFile and store every line into the string variable matrixRowString
    {
        std::stringstream matrixRowStringStream(matrixRowString); //convert matrixRowString that is a string to a stream variable.

        while (getline(matrixRowStringStream, matrixEntry, ',')) // here we read pieces of the stream matrixRowStringStream until every comma, and store the resulting character into the matrixEntry
        {
            matrixEntries.push_back(stod(matrixEntry)); //here we convert the string to double and fill in the row vector storing all the matrix entries
        }
        matrixRowNumber++; //update the column numbers
    }

    // here we convert the vector variable into the matrix and return the resulting object,
    // note that matrixEntries.data() is the pointer to the first memory location at which the entries of the vector matrixEntries are stored;
    return Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(matrixEntries.data(), matrixRowNumber, matrixEntries.size() / matrixRowNumber);
}

void Synergies::getXP()
{
    XP = openData("gaussXP.txt");
}



void Synergies::getMAP()
{
    MEP = XP * weights;
}

void Synergies::getWeights(float speed, float elevation)
{

    Eigen::MatrixXd baseline_val = (openData("weightings.txt")).transpose();

    Eigen::MatrixXd regression = openData("regression.txt");

    Eigen::MatrixXd coef;

    double temp1, temp2, correction;

    for (int c = 0; c < 4; c++)
    { 
     //comp_var = strcat('Comp', int2str(c));
     for (int m = 0; m < 15; m++){

         //muscle_var = strcat('muscle_', int2str(m));

         coef = regression.block(m,c*6,1,3);     
         temp1 = pow(coef(0,0) * 0,2);
         temp1 = temp1 + coef(0,1) * 0;
         temp1 = temp1 + coef(0,2);

         coef = regression.block(m, c * 6 +3, 1, 3);
         temp2 = pow(coef(0, 0) * 3, 2);
         temp2 = temp2 + coef(0, 1) * 3;
         temp2 = temp2 + coef(0, 2);

         correction = temp1 + temp2;

         coef = regression.block(m, c * 6, 1, 3);
         temp1 = coef(0, 0) * pow(elevation, 2);
         temp1 = temp1 + coef(0, 1) * elevation;
         temp1 = temp1 + coef(0, 2);

         coef = regression.block(m, c * 6 + 3, 1, 3);
         temp2 = coef(0, 0) * pow(speed, 2);
         temp2 = temp2 + coef(0,1) * speed;
         temp2 = temp2 + coef(0,2);

         weights(c, m) = (baseline_val(c, m) + temp1) + temp2 - correction;

         if (weights(c, m) < 0)
             weights(c, m) = 0; // deals with negative values.end
    }
}
}
