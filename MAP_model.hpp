//==============================================================
// Filename : MAP_model.hpp
// Authors : Federica Damonte
// Description :  Generates an estimate of Synergies and Muscle Excitation Profiles (MEP)
// Based on speed, elevation and baseline.
//
// There are 2 baseline modes: 'GM', which is the generic mode and no file
// should be indicated and 'SM', which is the subject-specific mode and
// a file should be indicated in the next argument (baseline_file)
//
// The function outpus and estimate of the weights and the XP for the
// synergies and the estimated MEPs.
//==============================================================

#ifndef __MAP_MODEL__HPP__
#define __MAP_MODEL__HPP__

#include "./eigen-3.3.9/eigen-3.3.9/Eigen/Eigen"

class Synergies {

    Eigen::MatrixXd XP, weights, MEP;

public:
    void getXP();
    void getWeights(float speed, float elevation);
    void getMAP();
};

#endif