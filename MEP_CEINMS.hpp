#ifndef MEP_CLASS
#define MEP_CLASS

#include <string>
#include "Bspline\Curve.hpp"
#include <vector>


class MEP_class
{
    std::vector<Curve<CurveMode::Offline>> gaussXP; //I need to define a curve for each motor component (primitive) that would be used to calculate the MEP

    std::vector<std::vector<double>> weightings; //for each muscle I would have weights for each motor component

    //import data
    std::vector<std::vector<double>> gaussianCoef;
    std::vector<std::vector<double>> regression_coef;
    std::vector<std::vector<double>> weightings_baseline;

public:
    //constructor

    MEP_class();

    //private:
    //double regression_coef[15][24];
    //double weightings_baseline[4][15];

    /* import the data from text files into vectors*/
    void importRegression_coef();
    //void importXP();
    void importBaseline();

    //Import functions from text files into vectors
    void import_gaussXP();
    //calculate the curves for each motor component
    void calcGaussians();

    //public:

    //double gaussXP[200][4];
    //double weightings[4][15];
    //double mep[200][15];

    
    /* calculates the model data for all walking cycle */
    void calcWeightings(int elevation, int speed);
    void calcMEP(int elevation, int speed, std::vector<double> &MEP, double time);

    //I need to get the MEP for every muscle at the given percentage of the gait cycle
    int getMuscle_Index(std::string muscle);
    double getMEP(int gaitCycle, std::string muscle);

    /* Gets the pointer to the const for the value (val = 1,2,3),
		 * component (comp = 1,2,3,4), cond ("elevation", "speed") */
    int w_pointer(std::string cond, int comp, int val);

    /* Save the output of the model in text files */
    //void saveMEP();
    //I do not need that
    // void saveXP();
    // void saveWeightings();
    // void saveMEP();
    // //void saveWeights();
    // void saveData(double data[]);
};

#endif
