//==============================================================
// Filename : main.cpp
// Authors : Federica Damonte
// Description :  Generates an estimate of Synergies and Muscle Excitation Profiles (MEP)
// Based on speed, elevation and baseline.
//
// There are 2 baseline modes: 'GM', which is the generic mode and no file
// should be indicated and 'SM', which is the subject-specific mode and
// a file should be indicated in the next argument (baseline_file)
//
// The function outpus and estimate of the weights and the XP for the
// synergies and the estimated MEPs.
//==============================================================

#include<string>
#include "MEP_CEINMS.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <numeric>

#include <stdlib.h>

void importMatlab(std::vector<std::vector<double>> &MatLabMEP)
{
    try
    {
        std::ifstream data("MEP.txt");
        //data.exceptions(ifstream::failbit | ifstream::badbit);
        int i = 0;
        int j = 0;
        std::string line;
        std::string cell;
        double cell_double = 0;
        //std::vector<std::vector<double>> MatLabMEP;

        // cout << "importing baseline" << endl;
        while (getline(data, line))
        {
            std::stringstream lineStream(line);
            std::string cell;
            std::vector<double> myVec;

            while (getline(lineStream, cell, ','))
            {
                cell_double = atof(cell.c_str());
                myVec.push_back(cell_double);
                //j++;
                //cout << j << " - " << i << endl;
            }
            MatLabMEP.push_back(myVec);
            myVec.erase(myVec.begin(), myVec.end());
            //i++;
            //j = 0;
        }
        data.close();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << e.what() << std::endl;
    }
}

int main(){

    std::cout << "Program to test how compute muscle synergies for given speed and elevations of the ground" << std::endl;

    // std::cout << "Program to compute muscle synergies for given speed and elevations of the ground" << std::endl;
    std::cout << "please note that the model is calculated only with speeds: 1 3 5 Km/h and elevations -20 0 20 %" << std::endl;

    MEP_class MySyn;

    int elevation = 20;
    int speed = 1;
    std::vector<double> MEP;
    std::vector<std::vector<double>> MatLabMEP;
    std::vector<std::vector<double>> CppMEP;
    double time ;

    MySyn.calcGaussians();



    for (int i = 1; i <= 200; i++){
        time = i * 0.5;
        MySyn.calcMEP(elevation, speed, MEP, time);
        // for (auto i:MEP) {
        //     std::cout << "MEP" << MEP[i] << std::endl;
        // }
        
        CppMEP.push_back(MEP);
        MEP.erase(MEP.begin(), MEP.end());
    }

    importMatlab(MatLabMEP);
    //MySyn.calcMEP(elevation, speed, MEP, time);

    //calculate the error between the matlab synergies and the one from the actual implementation
    std::vector<vector<double>> err;
    std::vector<double> myvec;
    double g;
    for (int i=0; i<200; i++) {

        for (int j=0; j<15; j++)
        {
         g = MatLabMEP[i][j] - CppMEP[i][j];
         
         myvec.push_back(g);

         std::cout << myvec[j] << std::endl;
         }
    err.push_back(myvec);
    myvec.erase(myvec.begin(), myvec.end());
    }
     
}